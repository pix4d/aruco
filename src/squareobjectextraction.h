#pragma once

#include <opencv2/core.hpp>
#include <vector>

namespace cv
{
    class Mat;
}

namespace aruco
{
    class CameraModel;

    /**
     * @brief extractSquareObject takes the area in input image inclosed by 4 points contour, undistorts it
     * if the camera internal parameters are known, removes the perspective distortion and returns the image of the
     * square object with required resolution
     * @param image input image
     * @param corners the image cooordinates of the inclosing contour. We expect 4 corners with anti-clockwise
     * orientation.
     * @param cameraModel pointer to camera internal parameters. Can be null.
     * @param squareSize the required width and height of result image
     * @param squareObject result image of the square object with required resolution. It is in the same image format
     * as the input image.
     * @return true if the extraction was successful, otherwise false. The reason for fail is bad number of
     * corners, wrong camera internals, or the corners are outside of the field of view
     */
    bool extractSquareObject(const cv::Mat& image, const std::vector<cv::Point2f>& corners,
                             const CameraModel* cameraModel,
                             int squareSize, cv::Mat& squareObject);
}
