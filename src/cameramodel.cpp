#include "cameramodel.h"

namespace aruco
{
    std::vector<cv::Point> CameraModel::raysProjection(const std::vector<cv::Point3f>& rays) const
    {
        if (hasDistortion())
        {
            std::vector<cv::Point> contour(rays.size());

            cv::Point2f p2;
            for (size_t i = 0; i < contour.size(); ++i)
            {
                if (!imagePointFromRay(rays[i], p2))
                {
                    contour.clear();
                    break;
                }

                contour[i].x = cvRound(p2.x);
                contour[i].y = cvRound(p2.y);
            }
            return contour;
        }
        else
        {
            return std::vector<cv::Point>();
        }
    }

    std::vector<cv::Point3f> CameraModel::contourRays(const std::vector<cv::Point2f>& contour) const
    {
        std::vector<cv::Point3f> rays(contour.size());
        for (size_t i = 0; i < rays.size(); ++i)
        {
            if (!rayFromImagePoint(contour[i], rays[i]))
            {
                rays.clear();
                break;
            }
        }

        return rays;
    }
}
