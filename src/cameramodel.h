#pragma once

#include "aruco_export.h"
#include <opencv2/core.hpp>

namespace aruco
{
    /**
     * @brief The CameraModel class contains the internal parameters of the camera. It is
     * used to compensate the distortion of the image. By default it is perspective
     * camera without distortion
     */
    class ARUCO_EXPORT CameraModel
    {
    public:
        /// Return the image width of the camera
        virtual int imageWidth() const = 0;

        /// Return the image height of the camera
        virtual int imageHeight() const = 0;

        virtual bool hasDistortion() const = 0;

        /**
         * @brief rayFromImagePoint Each image point is a projection of a set of 3D points,
         * all lying on a line. This line connects the 3D points with the camera
         * center and can be represent by a ray. This method computes the normalized ray.
         * @param p2 the image point
         * @param ray the normalized ray from the camera center to any of the 3D points
         */
        virtual bool rayFromImagePoint(const cv::Point2f& p2, cv::Point3f& ray) const = 0;

        /**
         * @brief imagePointFromRay Computes the 2D point where all points along the ray
         * project on the image
         * @param ray the normalized ray from the camera center to a point in 3D space
         * @param p2 the image point
         */
        virtual bool imagePointFromRay(const cv::Point3f& ray, cv::Point2f& p2) const = 0;

        /**
         * @brief contourRays Returns the rays for each image point from the countour.
         * @param contour a contour of image points
         * @return the vector of the normalized rays
         */
        std::vector<cv::Point3f> contourRays(const std::vector<cv::Point2f>& contour) const;

        /**
         * @brief raysProjection Returns the projection point on the image for
         * a set of rays
         * @param rays the normalized rays from the camera center to group of points in 3D spac
         * @return the vector of projection image points
         */
        std::vector<cv::Point> raysProjection(const std::vector<cv::Point3f>& rays) const;
    };
}
