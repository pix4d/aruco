#include "squareobjectextraction.h"
#include "cameramodel.h"

#include <opencv2/imgproc/imgproc.hpp>

/*****************************
extractSquareObject() undistorts an area on the input image enclosed by 4 image points which is expected
to be a square (planar) object. It uses the camera internal parameters if they are available.

The transformation from the image plane to the plane of the square object is homography. If the camera model
is known and the camera has distortion, we undistort the object on a projection plane and we apply homography
transformation afterward.

Default projection plane is the one in front of the camera. But for the cameras with bigger field of view,
the projection on left, right, top, bottom or back plane can have smaller perspective distortion. That's why
the projection plane is selected from the direction of the 3D rays for the contour's image points.
******************************/

namespace
{
    // It is not declared as enum class, because it is used for indexing
    enum ProjectionPlane
    {
        Front = 0,
        Right,
        Top,
        Back,
        Left,
        Bottom
    };

    // Return the 3D rays of image points computed for known camera internal parameters, if available
    std::vector<cv::Point3f> getRays(const std::vector<cv::Point2f>& imagePoints,
                                     int imageWidth,
                                     const aruco::CameraModel* cameraModel);

    // Checks if the camera distortion is visible for this polyline. The input arguments are 2D points of
    // the polyline in image resolution, the corresponding 3D rays of the polyine, the image resolution imageWidth
    // and the camera internal parameters in original camera resolution
    bool isPolylineDistorted(const std::vector<cv::Point2f>& imagePoints,
                             const std::vector<cv::Point3f>& rays,
                             int imageWidth,
                             const aruco::CameraModel* cameraModel);

    // Return clock-wise polyline of a square with certain dimension
    std::vector<cv::Point2f> squarePolyline(int squareSize);

    // Project the rays on a plane and return the perspective transformation from the the projection to
    // 2D square (squareSize x squareSize). squareRays should have 4 rays.
    cv::Mat getProjectionPerspectiveTransform(const std::vector<cv::Point3f>& squareRays,
                                              ProjectionPlane projectionPlane, int squareSize);

    // Project a ray on certain plane
    cv::Point2f projectOnPlane(const cv::Point3f& ray, ProjectionPlane projectionPlane);

    // Return the normalized ray for 2D point projected on certain plane
    cv::Point3f rayFromProjection(const cv::Point2f& projection, ProjectionPlane projectionPlane);

    // Find the best projection plane for 3D rays from the camera center
    ProjectionPlane findProjectionPlane(const std::vector<cv::Point3f>& rays);

    // Increase the count for the plane which is in front of the ray. Exclude the planes which are
    // parallel to the ray.
    void addPlaneIntersection(const cv::Point3f& ray, std::vector<int>& planeIntersectionCount);
}

namespace aruco
{
    bool extractSquareObject(const cv::Mat& image, const std::vector<cv::Point2f>& corners,
                             const CameraModel* cameraModel,
                             int squareSize, cv::Mat& squareObject)
    {
        if (corners.size() != 4)
        {
            return false;
        }

        // Get the rays of the corners
        const std::vector<cv::Point3f> cornerRays = getRays(corners, image.cols, cameraModel);

        if (!isPolylineDistorted(corners, cornerRays, image.cols, cameraModel))
        {
            // We do not have 3D information about scene.
            // Remove perspective distortion based on 2D points only
            const cv::Mat transformation = cv::getPerspectiveTransform(corners, squarePolyline(squareSize));
            cv::warpPerspective(image, squareObject, transformation,
                                cv::Size(squareSize, squareSize),
                                cv::INTER_NEAREST);
            return true;
        }

        // Project the rays on one of the 6 planes - front, back, left, right, top, bottom
        const ProjectionPlane projectionPlane = findProjectionPlane(cornerRays);

        const cv::Mat transformation = getProjectionPerspectiveTransform(cornerRays, projectionPlane, squareSize);

        // Create the result image of the square
        squareObject.create(squareSize, squareSize, image.type());

        const float scale = static_cast<float>(image.cols)/static_cast<float>(cameraModel->imageWidth());

        unsigned char pixel;
        cv::Point2f imagePointSrc; // the image coordinate in input image
        std::vector<cv::Point2f> imagePointDst(1), projectedPoint(1);
        for (int y = 0; y < squareSize; ++y)
        {
            for (int x = 0; x < squareSize; ++x)
            {
                //for each point on the result image find the corresponding pixel in input image
                imagePointDst[0].x = static_cast<float>(x);
                imagePointDst[0].y = static_cast<float>(y);

                // corresponding point on the projection plane
                cv::perspectiveTransform(imagePointDst, projectedPoint, transformation);

                const cv::Point3f ray = rayFromProjection(projectedPoint[0], projectionPlane);

                pixel = 0;
                if (cameraModel->imagePointFromRay(ray, imagePointSrc))
                {
                    const int r = cvRound(imagePointSrc.y*scale);
                    const int c = cvRound(imagePointSrc.x*scale);
                    if (r >= 0 && r < image.rows && c >= 0 && c < image.cols)
                    {
                        pixel = image.at<unsigned char>(r, c);
                    }
                }

                squareObject.at<unsigned char>(y, x) = pixel;
            }
        }
        return true;
    }
}

namespace
{
    std::vector<cv::Point2f> squarePolyline(int squareSize)
    {
        const float width = static_cast<float>(squareSize);

        std::vector<cv::Point2f> corners(4);
        corners[0] = cv::Point2f(0.0f, 0.0f);
        corners[1] = cv::Point2f(width - 1.0f, 0.0f);
        corners[2] = cv::Point2f(width - 1.0f, width - 1.0f);
        corners[3] = cv::Point2f(0.0f, width - 1.0f);
        return corners;
    }

    std::vector<cv::Point3f> getRays(const std::vector<cv::Point2f>& imagePoints,
                                     int imageWidth,
                                     const aruco::CameraModel* cameraModel)
    {
        if (cameraModel != nullptr && cameraModel->hasDistortion())
        {
            if(cameraModel->imageWidth() != imageWidth)
            {
                std::vector<cv::Point3f> rays(imagePoints.size());

                // The image and the camera model have different size.
                // Rescale the image points to get correct 3D rays
                const float scale = static_cast<float>(cameraModel->imageWidth())/static_cast<float>(imageWidth);

                for (size_t i = 0; i < rays.size(); ++i)
                {
                    if (!cameraModel->rayFromImagePoint(imagePoints[i]*scale, rays[i]))
                    {
                        rays.clear();
                        break;
                    }
                }
                return rays;
            }
            else
            {
                return cameraModel->contourRays(imagePoints);
            }
        }
        // Return empty vector, because we use the rays only to compensate known distortion
        return std::vector<cv::Point3f>();
    }

    bool isPolylineDistorted(const std::vector<cv::Point2f>& imagePoints,
                             const std::vector<cv::Point3f>& rays,
                             int imageWidth, const aruco::CameraModel* cameraModel)
    {
        if(rays.empty())
        {
            return false;
        }

        static const float cos5deg = 0.996f; // threshold for contour curvature

        const float scale = static_cast<float>(imageWidth)/static_cast<float>(cameraModel->imageWidth());

        size_t prev = rays.size() - 1;

        cv::Point3f middleRay;
        cv::Point2f middleImagePoint;
        for (size_t i = 0; i < rays.size(); ++i)
        {
            // Find the ray in the middle of two rays. It should project on line
            middleRay = rays[prev] + rays[i];
            middleRay /= cv::norm(middleRay);

            if (!cameraModel->imagePointFromRay(middleRay, middleImagePoint))
            {
                return false;
            }
            middleImagePoint *= scale; // scale to image resolution

            const cv::Point2f x1 = middleImagePoint - imagePoints[prev];
            const cv::Point2f x = imagePoints[i] - imagePoints[prev];
            if(x1.dot(x) < cv::norm(x1)*cv::norm(x)*cos5deg)
            {
                //line distortion: angle > 5 degree
                return true;
            }
            prev = i;
        }
        return false;
    }

    cv::Mat getProjectionPerspectiveTransform(const std::vector<cv::Point3f>& squareRays,
                                              ProjectionPlane projectionPlane, int squareSize)
    {
        std::vector<cv::Point2f> projectedPoints(squareRays.size());
        for (size_t i = 0; i < squareRays.size(); ++i)
        {
            projectedPoints[i] = projectOnPlane(squareRays[i], projectionPlane);
        }

        return cv::getPerspectiveTransform(squarePolyline(squareSize), projectedPoints);
    }

    void addPlaneIntersection(const cv::Point3f& ray, std::vector<int>& planeIntersectionCount)
    {
        ProjectionPlane intersectionPlane = ProjectionPlane::Front;
        float score = 0.0f; // how close is the ray to the normal of the intersection plane (0 to 1)

        // Check if planePos (plane for positive rayCoord) or planeNeg(plane for negative rayCoord)
        // is better intersection plane for the ray
        auto lookForPlaneIntersection = [&intersectionPlane, &score, &planeIntersectionCount]
                (float rayCoord, ProjectionPlane planePos, ProjectionPlane planeNeg)
        {
            if(planeIntersectionCount[planePos] >= 0)
            {
                // planes are not excluded already in order to avoid divide by zero
                const float absCosA = std::abs(rayCoord); // angle between the ray and plane normal
                if(absCosA < 1e-7)
                {
                    // Avoid divide by zero in projectOnPlane(). Exclude the parallel planes.
                    planeIntersectionCount[planePos] = -1;
                    planeIntersectionCount[planeNeg] = -1;
                }
                else if(absCosA > score)
                {
                    score = absCosA;
                    intersectionPlane = (rayCoord > 0.0f ? planePos : planeNeg);
                }
            }
        };

        lookForPlaneIntersection(ray.z, ProjectionPlane::Front, ProjectionPlane::Back);
        lookForPlaneIntersection(ray.x, ProjectionPlane::Right, ProjectionPlane::Left);
        lookForPlaneIntersection(ray.y, ProjectionPlane::Bottom, ProjectionPlane::Top);

        if(score > 0.0)
        {
            // There is an intersection plane.
            // Add one more ray intersection for this plane.
            planeIntersectionCount[intersectionPlane]++;
        }
    }

    ProjectionPlane findProjectionPlane(const std::vector<cv::Point3f>& rays)
    {
        // number of ray intersections with Front, Back, Left, Right, Top, Bottom planes
        std::vector<int> planeIntersectionCount(6, 0);

        for (const auto& ray : rays)
        {
            addPlaneIntersection(ray, planeIntersectionCount);
        }

        //find the plane with max ray intersections
        auto it = std::max_element(planeIntersectionCount.begin(), planeIntersectionCount.end());

        return static_cast<ProjectionPlane>(std::distance(planeIntersectionCount.begin(), it));
    }

    cv::Point2f projectOnPlane(const cv::Point3f& ray, ProjectionPlane projectionPlane)
    {
        switch (projectionPlane)
        {
        case ProjectionPlane::Front:
        default:
            return cv::Point2f(ray.x, ray.y) / ray.z;

        case ProjectionPlane::Back:
            return cv::Point2f(ray.y, ray.x) / ray.z;

        case ProjectionPlane::Right:
            return cv::Point2f(ray.y, ray.z) / ray.x;

        case ProjectionPlane::Left:
            return cv::Point2f(ray.z, ray.y) / ray.x;

        case ProjectionPlane::Bottom:
            return cv::Point2f(ray.z, ray.x) / ray.y;

        case ProjectionPlane::Top:
            return cv::Point2f(ray.x, ray.z) / ray.y;
        }
    }

    cv::Point3f rayFromProjection(const cv::Point2f& projection, ProjectionPlane projectionPlane)
    {
        // The length of the result ray before normalization
        const float norm = sqrt(projection.x*projection.x + projection.y*projection.y + 1.0f);

        switch (projectionPlane)
        {
        case ProjectionPlane::Front:
        default:
            return cv::Point3f(projection.x, projection.y, 1.0f)/norm;

        case ProjectionPlane::Back:
            return cv::Point3f(-projection.y, -projection.x, -1.0f)/norm;

        case ProjectionPlane::Right:
            return cv::Point3f(1.0f, projection.x, projection.y)/norm;

        case ProjectionPlane::Left:
            return cv::Point3f(-1.0f, -projection.y, -projection.x)/norm;

        case ProjectionPlane::Bottom:
            return cv::Point3f(projection.y, 1.0f, projection.x)/norm;

        case ProjectionPlane::Top:
            return cv::Point3f(-projection.x, -1.0f, -projection.y)/norm;
        }
    }
}
