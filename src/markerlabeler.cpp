#include "markerlabeler.h"
#ifdef USE_SVM_LABELER
#include "markerlabelers/svmmarkers.h"
#endif
#include "markerlabelers/dictionary_based.h"
namespace aruco
{
    MarkerLabelerParameters::MarkerLabelerParameters(float max_correction_rate,
                                                     float cellMarginRate,
                                                     bool biggerBlackMargin)
        : _max_correction_rate(max_correction_rate),
          _cellMarginRate(cellMarginRate),
          _biggerBlackMargin(biggerBlackMargin)
    {
    }

    cv::Ptr<MarkerLabeler> MarkerLabeler::create(Dictionary::DICT_TYPES dict_type,
                                                 const MarkerLabelerParameters& params)
    {
        Dictionary dict = Dictionary::loadPredefined(dict_type);
        DictionaryBased* db = new DictionaryBased();
        db->setParams(dict, params);
        return db;
    }

    cv::Ptr<MarkerLabeler> MarkerLabeler::create(const std::string& detector, const std::string& error_correction_rate,
                                                 float cellMarginRate, bool biggerBlackMargin)
    {
        if (detector == "SVM")
        {
#ifdef USE_SVM_LABELER
            SVMMarkers* svm = new SVMMarkers;
            if (!svm->load(error_correction_rate))
                throw cv::Exception(-1, "Could not open svm file :" + error_correction_rate, "Detector::create", " ", -1);
            //*SVMmodel,dictsize, -1, 1, true);
            return svm;
#else
            throw cv::Exception(-1, "SVM labeler not compiled", "Detector::create", " ", -1);
#endif
        }
        else
        {
            Dictionary dict = Dictionary::load(detector);
            // try with one from file
            DictionaryBased* db = new DictionaryBased();
            db->setParams(dict, MarkerLabelerParameters(std::stof(error_correction_rate), cellMarginRate, biggerBlackMargin));
            return db;
        }

        throw cv::Exception(-1, "No valid labeler indicated:" + detector, "Detector::create", " ", -1);
    }
}
