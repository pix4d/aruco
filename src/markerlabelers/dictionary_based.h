/*****************************
Copyright 2016 Rafael Muñoz Salinas. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Rafael Muñoz Salinas ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Rafael Muñoz Salinas OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Rafael Muñoz Salinas.
********************************/

#ifndef ArucoDictionaryBasedMarkerDetector_H
#define ArucoDictionaryBasedMarkerDetector_H

#include "../dictionary.h"
#include "../markerlabeler.h"

#include <opencv2/core/core.hpp>
namespace aruco
{
    /**Labeler using a dictionary
     */
    class DictionaryBased : public MarkerLabeler
    {
    public:
        DictionaryBased();

        virtual ~DictionaryBased()
        {
        }

        /**
         * @brief setParams sets the dictionary and settings for marker identification such as error correction
         * bits and cell margin
         * @param dic dictionary
         * @param params parameters for marker identification
         */
        void setParams(const Dictionary& dic, const MarkerLabelerParameters& params);

        // main virtual class to o detection
        bool detect(const cv::Mat& in, int& marker_id, int& nRotations) const override;
        // returns the dictionary name
        std::string getName() const;

        /**
         * @brief getBestInputSize if desired, you can set the desired input size to the detect function
         * @return -1 if detect accept any type of input, or a size otherwise
         */
        int getBestInputSize() const override;

        /**
         * @brief get number of bits on one side (sqrt of the total number of bits in a marker)
         * @return marker side size
         */
        int getMarkerSideSize() const override;

        /**
         * @brief get size of the black border in bits (thickness of black border on one side)
         * @return marker border size
         */
        int getMarkerBorderSize() const override;

    private:
        int getInnerSize() const;
        std::vector<uint64_t> getInnerCode(const cv::Mat& thres_img) const;
        Dictionary _dic;
        int _maxCorrectionAllowed;
        float _cellMarginRate;
        bool _biggerBlackMargin;
    };
}
#endif
