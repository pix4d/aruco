#include "dictionary_based.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <bitset>
#include <cmath>

namespace
{
    // Count the white bits in rectangle with offset (x0, y0), certain size and margin
    inline int countWhitePixels(const cv::Mat& bits, int x0, int y0, int size, int margin)
    {
        const cv::Mat square = bits(cv::Rect(x0 + margin,
                                             y0 + margin,
                                             size - 2*margin,
                                             size - 2*margin));
        return cv::countNonZero(square);
    }

    uint64_t touulong(const cv::Mat& code);
    cv::Mat rotate(const cv::Mat& in);
}


namespace aruco
{
    DictionaryBased::DictionaryBased()
        : _maxCorrectionAllowed(0),
          _cellMarginRate(MarkerLabelerParameters()._cellMarginRate),
          _biggerBlackMargin(MarkerLabelerParameters()._biggerBlackMargin)
    {
    }

    void DictionaryBased::setParams(const Dictionary& dic, const MarkerLabelerParameters& params)
    {
        _dic = dic;
        const float max_correction_rate = std::max(0.f, std::min(1.0f, params._max_correction_rate));
        _maxCorrectionAllowed = static_cast<int>( static_cast<float>(_dic.tau()) * max_correction_rate);
        _cellMarginRate = params._cellMarginRate;
        _biggerBlackMargin = params._biggerBlackMargin;
    }

    std::string DictionaryBased::getName() const
    {
        return aruco::Dictionary::getTypeString(_dic.getType());
    }

    int DictionaryBased::getBestInputSize() const
    {
        static const int CELL_SIZE_FOR_DETECTION = 8;   // The size of marker cells in pixels.
                                                        // It is the scale factor in marker detection step,
                                                        // which allows sub-pixel accuracy

        // scale the marker for the detection step
        return getMarkerSideSize() * CELL_SIZE_FOR_DETECTION;
    }

    int DictionaryBased::getMarkerSideSize() const
    {
        return getInnerSize() + _dic.borderSize() * 2;
    }

    int DictionaryBased::getMarkerBorderSize() const
    {
        return _dic.borderSize();
    }

    int DictionaryBased::getInnerSize() const
    {
        return static_cast<int>(std::sqrt(_dic.nbits()));
    }

    int hamm_distance(uint64_t a, uint64_t b)
    {
        return static_cast<int>(std::bitset<64>(a ^ b).count());
    }

    bool DictionaryBased::detect(const cv::Mat& in, int& marker_id, int& nRotations) const
    {
        assert(in.rows == in.cols);
        cv::Mat grey;
        if (in.type() == CV_8UC1)
            grey = in;
        else
            cv::cvtColor(in, grey, CV_BGR2GRAY);
        // threshold image
        cv::threshold(grey, grey, 125, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

        // get the ids in the four rotations (if possible)
        std::vector<uint64_t> ids = getInnerCode(grey);
        if (ids.empty())
        {
            return false;
        }

        // find the best one
        for (int i = 0; i < 4; i++)
        {
            if (_dic.is(ids[i]))
            {                    // is in the set?
                nRotations = i;  // how many rotations are and its id
                marker_id = _dic[ids[i]];
                return true;  // bye bye
            }
        }

        // you get here, no valid id :(
        // lets try error correction

        if (_maxCorrectionAllowed > 0)
        {  // find distance to map elements
            for (auto ci : _dic.getMapCode())
            {
                for (int i = 0; i < 4; i++)
                {
                    if (hamm_distance(ci.first, ids[i]) < _maxCorrectionAllowed)
                    {
                        marker_id = static_cast<int>(ci.second);
                        nRotations = i;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    std::vector<uint64_t> DictionaryBased::getInnerCode(const cv::Mat& thres_img) const
    {
        const int innerSize = getInnerSize();
        const int borderSize = static_cast<int>(_dic.borderSize());
        const int outerSize = innerSize + borderSize*2;
        // Markers  are divided in (innerSize+2*borderSize)x(innerSize+2*borderSize) regions, of which the inner bits_axbits_a belongs to marker
        // info
        // the external border should be entirely black

        // This is the size of the grid cell in pixels
        const float cellSize = static_cast<float>(thres_img.rows) / static_cast<float>(outerSize);
        const int swidth = static_cast<int>(cellSize);

        // This is the margin of the grid cell in pixels
        const int cellMarginWhite = static_cast<int>(cellSize*_cellMarginRate);
        const int cellMarginBlack = _biggerBlackMargin ? cellMarginWhite + 1 : cellMarginWhite;
        const int cellPixelsWhite = (swidth - 2*cellMarginWhite)*(swidth - 2*cellMarginWhite);
        const int cellPixelsBlack = (swidth - 2*cellMarginBlack)*(swidth - 2*cellMarginBlack);

        // Check if the border is black
        const int maxBorderErrors = (innerSize*2)/3; // threshold for allowed error bits in the border
        int whiteBorderCells = 0;
        for (int y = 0; y < outerSize; y++)
        {
            const int y0 = static_cast<int>(static_cast<float>(y)*cellSize);

            for (int x = 0; x < outerSize; x++)
            {
                const int x0 = static_cast<int>(static_cast<float>(x)*cellSize);

                // The cell is white if more than 50% of the pixels are white
                if (countWhitePixels(thres_img, x0, y0, swidth, cellMarginWhite)*2 > cellPixelsWhite)
                {
                    whiteBorderCells++;
                    if(whiteBorderCells > maxBorderErrors)
                    {
                        // can not be a marker because too many border cells are not black!
                        return std::vector<uint64_t>();
                    }
                }
                if (x == borderSize - 1 && y >= borderSize && y < outerSize - borderSize)
                {
                    // skip the inner cells for all rows which are not pure border
                    x = outerSize - borderSize - 1;
                }
            }
        }

        // now,
        // get information(for each inner square, determine if it is  black or white)
        cv::Mat _bits = cv::Mat::zeros(innerSize, innerSize, CV_8UC1);

        for (int y = 0; y < innerSize; y++)
        {
            const int y0 = static_cast<int>(static_cast<float>(y + borderSize) * cellSize);

            for (int x = 0; x < innerSize; x++)
            {
                const int x0 = static_cast<int>(static_cast<float>(x + borderSize) * cellSize);

                // Check if the pixel in the center of the cell is black or white
                const bool blackCenter = (thres_img.at< uchar >(y0 + swidth/2, x0 + swidth/2) == 0);

                const int whitePixels = countWhitePixels(thres_img, x0, y0, swidth,
                                                         blackCenter ? cellMarginBlack : cellMarginWhite);
                // Set the bit to one if more than 50% of the cell is white
                if (whitePixels*2 > (blackCenter ? cellPixelsBlack : cellPixelsWhite))
                {
                    _bits.at< uchar >(y, x) = 1;
                }
            }
        }

        // now, get the 64bits ids
        std::vector<uint64_t> ids;
        int nRotations = 0;
        do
        {
            ids.push_back(touulong(_bits));
            _bits = rotate(_bits);
            nRotations++;
        } while (nRotations < 4);
        return ids;
    }
}

namespace
{
    // convert matrix of (0,1)s in a 64 bit value
    uint64_t touulong(const cv::Mat& code)
    {
        std::bitset<64> bits;
        int bidx = 0;
        for (int y = code.rows - 1; y >= 0; y--)
            for (int x = code.cols - 1; x >= 0; x--)
                bits[bidx++] = code.at<uchar>(y, x);
        return bits.to_ullong();
    }

    cv::Mat rotate(const cv::Mat& in)
    {
        cv::Mat out;
        in.copyTo(out);
        for (int i = 0; i < in.rows; i++)
        {
            for (int j = 0; j < in.cols; j++)
            {
                out.at<uchar>(i, j) = in.at<uchar>(in.cols - j - 1, i);
            }
        }
        return out;
    }
}
